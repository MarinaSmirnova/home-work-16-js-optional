function numbersFibonacci(n) {
    let f0 = 0;
    let f1 = 1;

    if (n === 0) { return 0; }
    if (Math.sign(n) === -1) {  // Якщо n - від'ємне
        for (let i = -2; i >= n; i--) {

            sum = f0 - f1;
            f0 = f1;
            f1 = sum;
        }
        return f1;

    } if (Math.sign(n) === 1) {  // Якщо n - додатне

        for (let i = 2; i <= n; i++) {

            sum = f0 + f1;
            f0 = f1;
            f1 = sum;
        } 
        return f1;
    }
}

let n = +prompt("Enter n");
alert(numbersFibonacci(n)); 

